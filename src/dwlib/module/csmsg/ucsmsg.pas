unit ucsmsg;

interface
uses 
  Windows, SysUtils, Variants, Classes, ubase, utypes, uencode;

type
  { 字段定义
  }
  PCSMsgField = ^TCSMsgField;
  TCSMsgField = record
    len : word;
    pvalue : pchar;
  end;

  { 报文格式定义
  }
  TCSMsg = class(tbase)
  private
    function b_xor(const buffer : pchar; len : integer) : byte;
    //异或和
    function b_and(const buffer : pchar; len : integer) : byte;
    //累加和
  public
    constructor create();
    destructor Destroy(); override;
  public
    function pack(buffer : pchar; var len : integer) : boolean;
    function unpack(const buffer : pchar; len : integer) : boolean;
  public
    function makefield(const buffer : pchar; ilen : integer) : PCSMsgField;
    function addfield(msgfield : PCSMsgField) : boolean;
    procedure clearfields();
  public
    msglen : integer;
    command : byte;
    return_code : Byte;
    fieldlist : TList;
    //字段列表
end;


implementation

{ TCSMsg }

function TCSMsg.addfield(msgfield : PCSMsgField): boolean;
begin
  fieldlist.Add(msgfield);
  Result := True;
end;

function TCSMsg.b_and(const buffer : pchar; len: integer): byte;
var
  i : integer;
begin
  result := byte(buffer[0]);
  
  for i:=1 to len-1 do
  begin
    result := result and byte(buffer[i]);
  end;
end;

function TCSMsg.b_xor(const buffer : pchar; len: integer): byte;
var
  i : integer;
begin
  result := byte(buffer[0]);
  
  for i:=1 to len-1 do
  begin
    result := result xor byte(buffer[i]);
  end;
end;

procedure TCSMsg.clearfields;
var
  msgfield : PCSMsgField;
  i : integer;
begin
  for i:=0 to fieldlist.Count-1 do
  begin
    msgfield := fieldlist.Items[i];
    freemem(msgfield^.pvalue);
    freemem(msgfield);
  end;
  fieldlist.Clear;
end;

constructor TCSMsg.create;
begin
  fieldlist := TList.create();
end;

destructor TCSMsg.Destroy;
begin
  //清空字段列表
  clearfields;
  
  //销毁字段链表
  FreeAndNil(fieldlist);
end;

function TCSMsg.makefield(const buffer: pchar; ilen: integer): PCSMsgField;
var
  msgfield : PCSMsgField;
begin
  getmem(msgfield, sizeof(TCSMSGfield));
  msgfield^.len := ilen;
  getmem(msgfield^.pvalue, msgfield^.len+1);
  ZeroMemory(msgfield^.pvalue, msgfield^.len+1);
  copymemory(msgfield^.pvalue, buffer, ilen);
  result := msgfield;
end;

function TCSMsg.pack(buffer : pchar; var len: integer): boolean;
var
  i : integer;
  bxor, band : byte;
  msgfield : PCSMsgfield;
begin
  msglen := 0;
  //包头
  buffer[msglen] := chr($FF);
  msglen := msglen + 1;
  buffer[msglen] := chr($FF);
  msglen := msglen + 1;

  //数据包总长度
  msglen := msglen + 2;

  //命令字    
  buffer[msglen] := chr(command); 
  msglen := msglen + 1;

  //交易返回码
  buffer[msglen] := chr(return_code);
  msglen := msglen + 1;

  //可变数据部分
  for i:=0 to fieldlist.Count-1 do
  begin
    msgfield := fieldlist.Items[i];
    copymemory(@buffer[msglen], @msgfield^.len, 2);
    msglen := msglen + 2;
    copymemory(@buffer[msglen], msgfield^.pvalue, msgfield^.len);
    msglen := msglen + msgfield^.len;
  end;

  //数据包总长度
  msglen := msglen + 1; //异或和
  msglen := msglen + 1; //累加和
  copymemory(@buffer[2], @msglen, 2);

  //校验码
  bxor := b_xor(buffer, msglen);
  band := b_and(buffer, msglen);
  buffer[msglen-1] := chr(bxor);
  buffer[msglen-2] := chr(band);

  len := msglen;

  Result := True;
end;

function TCSMsg.unpack(const buffer : pchar; len: integer): boolean;
var
  i : integer;
  msglen : word;
  bxor, band : byte;
  msgfield : PCSMsgField;
  p : pchar;
begin
  Result := False;
  
  p := buffer;

  //缓冲区长度验证
  if len < 7 then
  begin
    errcode := -1;
    errmsg := '包长度太短';
    exit;
  end;

  //包头验证
  if (byte(p[0]) <> $FF) or
     (byte(p[1]) <> $FF) then
  begin
    errcode := -1;
    errmsg :='包头错误';
    exit;
  end;

  //包校验
  bxor := b_xor(buffer, len-1);
  band := b_and(buffer, len-2);
  if (bxor <> byte(p[len-1])) or
     (band <> byte(p[len-2])) then
  begin
    errcode := -1;
    errmsg :='包校验失败';
    exit;
  end;

  //包长度
  copymemory(@msglen, @buffer[2], 2);
  if msglen <> len then
  begin
    errcode := -2;
    errmsg :='包长度不相符';
    exit;
  end;

  //命令字
  command := byte(p[4]);
  if (command <> $01) and (command <> $02) and (command <$03) then
  begin
    errcode := -2;
    errmsg :='非法的命令字';
    exit;
  end;

  //交易返回码
  return_code := Byte(p[5]);

  //分解字段
  i := 6;
  while i<len-2 do
  begin
    getmem(msgfield, sizeof(TCSMsgField));
    copymemory(@msgfield^.len, @buffer[i], 2);
    i := i+2;
    getmem(msgfield^.pvalue, sizeof(char)*msgfield^.len);
    copymemory(msgfield^.pvalue, @buffer[i], msgfield^.len);
    i := i+msgfield^.len;
    fieldlist.Add(msgfield);
  end;

  Result := True;     
end;

end.

{ 调用例子
procedure Tfrmmain.Button1Click(Sender: TObject);
var
  msgfield : PCSMsgField;
  csmsg : TCSMsg;
  buf : array[0..1024] of char;
  msglen : integer;
begin
  inherited;

  csmsg := TCSMsg.create;
  csmsg.addfield(csmsg.makefield('field one', 9));
  csmsg.addfield(csmsg.makefield('field2', 6));
  csmsg.command := 1;
  csmsg.pack(buf, msglen);

  udpclient.SendBuffer('127.0.0.1', 8008, buf, msglen);

end;
}
