//*******************************************************************
//  pmyhisgroup  pcxbb_yaok   
//  umyhisreport.pas     
//                                          
//  创建: changym by 2012-05-06 
//        changup@qq.com                    
//  功能说明:                                            
//      封装窗体报表信息
//  修改历史:
//                                            
//  版权所有 (C) 2012 by changym
//                                                       
//*******************************************************************
unit umyhisreport;

interface

uses    
  SysUtils, Classes, Math, Types, DateUtils, udbbase, utypes, ADODB, frxClass,
  frxDBSet, frxVariables;

type
  { 报表变量类定义 }
  PMyHisReportVariable = ^TMyHisReportVariable;
  TMyHisReportVariable = class(TDbBase)
  public
    bianm : Integer;    //编码
    baobbm : Integer;   //报表编码
    bianlmc : string;   //变量名称
    bianlms : string;   //变量描述
    paixh : integer;    //排序号
    shujkjmc : string;  //数据集控件名称
    laiyzdmc : string;  //来源字段名称
  public
    function getVariableInfo(in_bianm : integer) : Boolean;
    //获取变量详细信息
  end;

  { 报表数据集定义
  }
  TMyHisReportDataset = class(TDbBase)
  public
    bianm : Integer;      //编码
    baobbm : integer;     //报表编码
    shujjyhmc : string;     //数据集名称,TfrxDBDataset控件的UserName属性;
    shujjsql : string;    //数据集sql
    shujjms : string;     //数据集描述
    paixh : Integer;      //排序号
    shujkjmc : string;    //数据控件名称
    dayjlksd : integer;   //打印记录开始点
    dayjljsd : Integer;   //打印记录结束点
    dayjlts : Integer;    //打印记录条数
  public
    function getDataSetInfo(in_bianm : integer) : Boolean;
    //获取数据集详细信息
  end;


  { 报表定义
  }
  PMyHisReport = ^TMyHisReport;
  TMyHisReport = class(TDbBase)
  private    
  { 字段定义 }
  public
    bianm : Integer;    //编码
    mokbm : string;    //归属模块编码
    chuangtmc : string; //窗体名称
    chuangtms : string; //窗体描述
    baobmc : string;    //报表名称
    baobms : string;    //报表描述

    //数据集list
    datasetlist : TList;
    //变量list
    variablelist : TList;

  public
    function getReportInfo(in_bianm : integer) : Boolean;
    //获取报表信息;
    function getVariable(strvarname : string; var variable : TMyHisReportVariable) : Boolean;
  public
    constructor create(pcon : PUpAdoConnection);
    destructor destroy(); override;
  public

  end;

  { 窗体报表集合定义
  }
  TMyHisReportList = class(TDbBase)
  public
    reportlist : TList;
  public
    function getReportInfoByFormName(strFormName : String) : Boolean;
    //根据窗体名获取报表信息;

    function getReportByName(strReportName : string;
        var preport : TMyHisReport) : Boolean;
    //根据报表名称查找报表对象
  public   
    constructor Create(padocon : PUpAdoConnection);
    destructor Destroy(); override;
  end;

implementation

{ TReport }
constructor TMyHisReport.create(pcon: PUpAdoConnection);
begin
  inherited Create(pcon);
  
  //创建数据集list
  datasetlist := TList.Create;
  //创建变量list
  variablelist := TList.Create;
end;

destructor TMyHisReport.destroy;
var
  i : Integer;
begin
  //销毁数据集list
  for i:=0 to datasetlist.Count-1 do
  begin
    TMyHisReportDataset(datasetlist.Items[i]).Free;
  end;
  datasetlist.Clear;
  datasetlist.Free;
  datasetlist := nil;

  //销毁变量list
  for i:=0 to variablelist.Count-1 do
  begin
    TMyHisReportVariable(variablelist.Items[i]).Free;
  end;
  variablelist.Clear;
  variablelist.Free;
  variablelist := nil;

  inherited destroy();
end;

function TMyHisReport.getReportInfo(in_bianm : integer): Boolean;
var
  rpdataset : TMyHisReportDataset;
  rpvariable : TMyHisReportVariable;
begin
  Result := False;

  { 取报表信息
  }
  //查找报表
  qry.Close;
  qry.SQL.Text := 'select * from jc_baobdy where bianm=' + IntToStr(in_bianm);
  qry.Open;
  if qry.RecordCount = 0 then
  begin
    errcode := -1;
    errmsg := '指定的报表' + IntToStr(in_bianm) + '未定义！';
    qry.Close;
    Exit;
  end;
  //字段赋值
  bianm := qry.fieldbyname('bianm').AsInteger;
  mokbm := qry.fieldbyname('mokbm').AsString;
  chuangtmc := qry.fieldbyname('chuangtmc').AsString;
  chuangtms := qry.fieldbyname('chuangtms').AsString;
  baobmc := qry.fieldbyname('baobmc').AsString;
  baobms := qry.fieldbyname('baobms').AsString;
  qry.Close;

  //报表数据集定义
  qry.Close;
  qry.SQL.Text := 'select bianm from jc_baobsjj' +
      ' where baobbm=' + IntToStr(bianm) +
      ' order by paixh';
  qry.Open;
  while not qry.Eof do
  begin
    rpdataset := TMyHisReportDataset.Create(fpadocon);
    if not rpdataset.getDataSetInfo(qry.fieldbyname('bianm').AsInteger) then
    begin
      errcode := -1;
      errmsg := '获取报表数据集定义发生错误：' + rpdataset.errmsg;
      Exit;
    end;
    datasetlist.Add(rpdataset);
    
    qry.Next;
  end;

  //报表变量定义
  qry.Close;
  qry.SQL.Text := 'select bianm from jc_baobbl' +
      ' where baobbm=' + IntToStr(bianm) +
      ' order by paixh';
  qry.Open;
  while not qry.Eof do
  begin
    rpvariable := TMyHisReportVariable.Create(fpadocon);
    if not rpvariable.getVariableInfo(qry.fieldbyname('bianm').AsInteger) then
    begin
      errcode := -1;
      errmsg := '获取报表变量定义发生错误：' + rpvariable.errmsg;
      Exit;
    end;
    variablelist.Add(rpvariable);

    qry.Next;
  end;
  qry.Close;

  Result := True;
end;

{ TMyHisReportList }

constructor TMyHisReportList.Create(padocon: PUpAdoConnection);
begin
  inherited Create(padocon);
  
  //创建报表list容器
  reportlist := Tlist.Create;  
end;

destructor TMyHisReportList.Destroy;
var
  i : integer;
begin
  for i:=0 to reportlist.Count-1 do
  begin
    TMyHisReport(reportlist.Items[i]).Free;
  end;
  reportlist.Clear;
  reportlist.Free;
  reportlist := nil;
  
  inherited;
end;

function TMyHisReportList.getReportByName(strReportName: string;
  var preport: TMyHisReport): Boolean;
var
  i : Integer;
  bfound : Boolean;
begin
  bfound := False;
  
  for i:=0 to reportlist.Count-1 do
  begin
    if UpperCase(TMyHisReport(reportlist.Items[i]).baobmc) =
        UpperCase(strReportName) then
    begin
      //找到报表对象
      preport := TMyHisReport(reportlist.Items[i]);
      bfound := True;
      Break;
    end;
  end;
  
  Result := (bfound = True);
end;

function TMyHisReportList.getReportInfoByFormName(
  strFormName: String): Boolean;
var
  rpreport : TMyHisReport;
begin
  Result := False;

  { 取报表信息
  }
  //查找报表;
  qry.Close;
  qry.SQL.Text := 'select bianm from jc_baobdy where chuangtmc=''' + strFormName + '''';
  qry.Open;
  while not qry.Eof do
  begin
    rpreport := TMyHisReport.create(fpadocon);
    if not rpreport.getReportInfo(qry.fieldbyname('bianm').AsInteger) then
    begin
      errcode := -1;
      errmsg := '获取报表信息发生错误：' + rpreport.errmsg;
      Exit;
    end;
    reportlist.Add(rpreport);

    qry.Next;
  end;
  
  Result := True;
end;

{ TMyHisReportDataset }

function TMyHisReportDataset.getDataSetInfo(in_bianm: integer): Boolean;
begin
  Result := False;
  
  { 取报表数据集信息
  }
  qry.Close;
  qry.SQL.Text := 'select * from jc_baobsjj where bianm=' + IntToStr(in_bianm);
  qry.Open;
  if qry.RecordCount = 0 then
  begin
    errcode := -1;
    errmsg := '指定的报表数据集' + IntToStr(in_bianm) + '未定义！';
    qry.Close;
    Exit;
  end;

  bianm := qry.fieldbyname('bianm').AsInteger;
  baobbm := qry.fieldbyname('baobbm').AsInteger;
  shujjyhmc := qry.fieldbyname('shujjyhmc').AsString;
  shujjsql := qry.fieldbyname('shujjsql').AsString;
  shujjms := qry.fieldbyname('shujjms').AsString;
  paixh := qry.fieldbyname('paixh').AsInteger;
  shujkjmc := qry.fieldbyname('shujkjmc').AsString;
  dayjlksd := qry.fieldbyname('dayjlksd').AsInteger;
  dayjljsd := qry.fieldbyname('dayjljsd').AsInteger;
  dayjlts := qry.fieldbyname('dayjlts').AsInteger;
  qry.Close;

  Result := True;
end;

{ TMyHisReportVariable }

function TMyHisReportVariable.getVariableInfo(in_bianm: integer): Boolean;
begin
  Result := False;

  qry.Close;
  qry.SQL.Text := 'select * from jc_baobbl where bianm=' + IntToStr(in_bianm);
  qry.Open;
  if qry.RecordCount = 0 then
  begin
    errcode := -3;
    errmsg := '指定报表变量' + IntToStr(in_bianm) + '未定义！';
    qry.Close;
    Exit;
  end;

  bianm := qry.fieldbyname('bianm').AsInteger;
  baobbm := qry.fieldbyname('baobbm').AsInteger;
  bianlmc := qry.fieldbyname('bianlmc').AsString;
  bianlms := qry.fieldbyname('bianlms').AsString;
  paixh := qry.fieldbyname('paixh').AsInteger;
  shujkjmc := qry.fieldbyname('shujkjmc').AsString;
  laiyzdmc := qry.fieldbyname('laiyzdmc').AsString;
  qry.Close;

  Result := True;
end;

function TMyHisReport.getVariable(strvarname: string;
  var variable: TMyHisReportVariable): Boolean;
var
  i : Integer;
  bfound : Boolean;
begin
  bfound := False;

  for i:=0 to variablelist.Count-1 do
  begin
    if UpperCase(TMyHisReportVariable(variablelist.Items[i]).bianlmc) =
        UpperCase(strvarname) then
    begin
      variable := TMyHisReportVariable(variablelist.Items[i]);
      bfound := True;
      Break;
    end;
  end;

  Result := (bfound = True);
end;

end.
