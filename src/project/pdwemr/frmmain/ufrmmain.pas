unit ufrmmain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ImgList, StdCtrls, ExtCtrls, upsoft, DateUtils,
  IniFiles, ComCtrls, ToolWin, Buttons, UpSpeedButton, UpPageControl,
  UPanel, Grids, DB, ADODB, udlltransfer, uglobal, AdvOfficeTabSet,
  ufrmcomm_process, ulog, AdvMenus,
  UpImage, AdvOfficeTabSetStylers, AdvTabSet,
  cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinsdxNavBar2Painter, UpImageList, dxNavBarCollns,
  cxClasses, dxNavBarBase, dxNavBar, utblyuang, udllfunctions,utypes,
  dxSkinsdxBarPainter, dxBar, jpeg, RzPanel, shellapi, umysystem;

type
  PDxNavBar = ^TDxNavBar;

  Tfrmmain = class(TForm)
    stbmain: TStatusBar;
    bvl1: TBevel;
    officetabsetstyle1: TAdvOfficeTabSetOfficeStyler;
    mditab1: TAdvOfficeMDITabSet;
    pmmditabpopup: TPopupMenu;
    nmditab_close_curr: TMenuItem;
    nmditab_close_other: TMenuItem;
    nmditab_close_all: TMenuItem;
    pmmditablist: TPopupMenu;
    upmglst_buttons_common: TUpImageList;
    imglstLargeImages: TImageList;
    navbar: TdxNavBar;
    navbgyewgl: TdxNavBarGroup;
    navbgjicsj: TdxNavBarGroup;
    navbgshujzd: TdxNavBarGroup;
    navbgxitgl: TdxNavBarGroup;
    dxnvbrgrpcntrlControl: TdxNavBarGroupControl;
    navyewgl: TdxNavBar;
    navbg1: TdxNavBarGroup;
    navbg_yewblGroup1: TdxNavBarGroup;
    dxnvbrgrpcntrlControl12: TdxNavBarGroupControl;
    dxnvbrgrpcntrlControl13: TdxNavBarGroupControl;
    navxitgl: TdxNavBar;
    navbg40: TdxNavBarGroup;
    navjicsj: TdxNavBar;
    navbg37: TdxNavBarGroup;
    dxnvbrgrpcntrlControl1: TdxNavBarGroupControl;
    navshujzd: TdxNavBar;
    navbg3: TdxNavBarGroup;
    imglstnavbar: TImageList;
    dxBarManagerBarManager: TdxBarManager;
    dxbrBarManagerBar1: TdxBar;
    dxbrBarManagerBar2: TdxBar;
    toolbsyslock: TdxBarLargeButton;
    toolbBarButtonExit: TdxBarLargeButton;
    btndxbrbtn3: TdxBarButton;
    btndxbrbtn4: TdxBarButton;
    toolbwinlist: TdxBarLargeButton;
    dxbrsbtmFile: TdxBarSubItem;
    dxbrsbtmEdit: TdxBarSubItem;
    dxbrsbtmFormat: TdxBarSubItem;
    dxbrsbtmWindow: TdxBarSubItem;
    dxbrsbtmHelp: TdxBarSubItem;
    dxbrgrp1: TdxBarGroup;
    upmglst_toolbar: TUpImageList;
    toolb1: TdxBarLargeButton;
    btn1: TdxBarButton;
    toolb2: TdxBarLargeButton;
    navbgchaxbb: TdxNavBarGroup;
    toolb3: TdxBarLargeButton;
    btn2: TdxBarButton;
    bvltop: TBevel;
    ptopspace: TPanel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure mnquitClick(Sender: TObject);
    procedure nmditab_close_currClick(Sender: TObject);
    procedure nmditab_close_otherClick(Sender: TObject);
    procedure nmditab_close_allClick(Sender: TObject);
    procedure mditab1TabClose(Sender: TObject; TabIndex: Integer;
      var Allow: Boolean);
    procedure btn1Click(Sender: TObject);
    procedure btnquitClick(Sender: TObject);
    procedure toolb1Click(Sender: TObject);
    procedure toolbBarButtonExitClick(Sender: TObject);
    procedure mditab1Change(Sender: TObject);
    procedure toolb2Click(Sender: TObject);
    procedure toolb3Click(Sender: TObject);
  private        
    dllparams : TDllParam;    
    dmyuang : TTblYuang;
                            
    GPrevShortDate,GPrevLongDate,GPrevTimeFormat:string;
    //保存的系统启动前的日期格式
    
    procedure appendChildForm(form : TForm);
    procedure showSystemMessage(strmessage : string);
    
    procedure closeNavBar();
    procedure openNavBar();

    procedure initNavBar();
    //初始化导航栏;

    //根据db配置创建菜单项3级菜单
    procedure addNavBarItems(moksy,zusy : integer; pnavbar : PDxNavBar);
    //新增菜单函数
    procedure OnNavBarItemClick(Sender : TObject);
    //菜单项单击事件处理
    procedure MokNavCanHide(mokIndex : integer; pnavbar : PDxNavBar);
    //检查模块nav分组是否可以隐藏；
    //如果当前角色当前模块没有一个菜单可操作，模块整个隐藏

  private
    procedure upmessage_leftnavbar_close(var msg : TMSG); message UPM_FRMMAIN_LEFTNAVBAR_CLOSE;
    { 其他窗体通知关闭左侧导航栏消息
      frmmainBG在进入业务窗体后通知主窗体关闭导航栏
    }
    
    procedure upmessage_mdichild_close(var msg : TMSG); message UPM_MDICHILD_ONCLOSE;
    { mdichild窗体关闭消息
      //子窗体关闭的时候会向主窗体发送自定义消息 UPM_MDICHILD_ONCLOSE
    }
    
  private
    procedure getSysDatetimeFormat();
    //获取系统日期格式
    procedure restoreSysDatetimeFormat();
    //恢复系统日期格式，在退出的时候还原
    procedure setSysDatetimeFormat();
    //设置系统日期格式

  public
    function init() : Boolean;
  end;

var
  frmmain: Tfrmmain;

implementation

uses ufrmxitgl_xiugkoul, ufrmmainBG;

{$R *.dfm}

procedure Tfrmmain.FormClose(Sender: TObject; var Action: TCloseAction);
var
  i : Integer;
  item : TMenuItem;
begin
  if self.MDIChildCount > 1 then
  begin
    ShowMessage('有业务子窗体未关闭,请先关闭所有业务窗口！');
    Action := caNone;
    Exit;
  end;

  if MessageDlg('确认关闭系统吗？', mtInformation, [mbYes, mbNo], 0) <> mrYes then
  begin
    Action := caNone;
    Exit;
  end;

  if Assigned(log) then
    FreeAndNil(log);
end;

function Tfrmmain.init: Boolean;
var
  appname, appverion : string;   
  strfilename : string;
  pdfver: string;
  odbcsource, dbuser, dbpass, dbdatabase, dbserver: string;
begin
  //日志文件
  log := TLog.Create('iysgl');

  dllparams.papp := @Application;
  dllparams.mysystem := @gmysystem;
  dllparams.padoconn := gmysystem.fpadocon;
  
  //20170203新增加随处可用的子窗体添加功能
  gmysystem.FAppendChildForm := appendChildForm;
  gmysystem.FShowSystemMessage := showSystemMessage;

  //从配置文件读取app和cliend信息；
  appname := gmysystem.mainconfig.ReadString('system','appname','');
  appverion := gmysystem.mainconfig.ReadString('system','appverion','');
  frmmain.Caption := appname + ' ' + appverion;
  Application.Title := appname;
  stbmain.Panels[0].Text := ' 欢迎使用' + appname;
  stbmain.Panels[1].Text := '当前用户：' + dllparams.mysystem.loginyuang.xingm;
  stbmain.Panels[2].Text := '角色：' + dllparams.mysystem.loginyuang.jiaosmc;

  dllparams.mysystem.cliendid := gmysystem.mainconfig.ReadString('system','cliendid','');
  dllparams.mysystem.cliendname := gmysystem.mainconfig.ReadString('system','cliendname','');

  dmyuang := TTblYuang.Create(gmysystem.fpadocon);

  { 新版导航栏增加 changym add by 2016-08-06
  }
  initNavBar;
                  
  //toolbar背景图
  strfilename := format('%s\toolbar_background_%d.bmp',
    [ExtractFilePath(application.ExeName), screen.Width]);
  if not FileExists(strfilename) then
  begin
    strfilename:= ExtractFilePath(application.ExeName) + 'toolbar_background.bmp';
  end;
  dxBarManagerBarManager.Backgrounds.Bar.LoadFromFile(strfilename);

  { 导航面板窗体
  }
  if not assigned(frmmainBG) then
    frmmainBG := tfrmmainBG.Create(nil);
  frmmainBG.ffrmmainhandle := self.Handle;
  frmmainBG.Execute_dll_show(@dllparams);
  //frmmainBG.changeNav(navbar.Groups[navbar.ActiveGroupIndex].Caption);
  dllparams.mysystem^.FAppendChildForm(frmmainBG);  

  //保存系统启动前的日期格式
  getSysDatetimeFormat();

  //设置为我们需要的格式
  setSysDatetimeFormat();

  stbmain.Panels[3].Text := '当前服务：' + dbserver;
            
  //关闭导航条
  closeNavBar();
  Application.ProcessMessages;
end;

procedure Tfrmmain.mnquitClick(Sender: TObject);
begin
  close;
end;

procedure Tfrmmain.appendChildForm(form: TForm);
begin
  mditab1.Visible := True;
  mditab1.AddTab(form);

  Application.ProcessMessages;
end;

procedure Tfrmmain.nmditab_close_currClick(Sender: TObject);
begin
  //关闭当前活动窗口
  if mditab1.AdvOfficeTabCount <= 0 then Exit;

  mditab1.GetChildForm(mditab1.AdvOfficeTabs[mditab1.ActiveTabIndex]).Close;     
  if self.MDIChildCount <= 1 then
  begin
    mditab1.Visible := False;
  end;
end;

procedure Tfrmmain.nmditab_close_otherClick(Sender: TObject);
var
  i : Integer;
begin
  //关闭当前活动窗口；
  if mditab1.AdvOfficeTabCount <= 0 then Exit;

  //轮训所有的窗口，跳过当前活动窗口；
  for i:=0 to mditab1.AdvOfficeTabCount-1 do
  begin
    //跳过当前活动窗口；
    if i <> mditab1.ActiveTabIndex then
    begin
      mditab1.GetChildForm(mditab1.AdvOfficeTabs[i]).Close;
    end;
  end;
end;

procedure Tfrmmain.nmditab_close_allClick(Sender: TObject);
var
  i : Integer;
begin   
  //关闭当前活动窗口；
  if mditab1.AdvOfficeTabCount <= 0 then Exit;

  //关闭所有的窗口；
  for i:=0 to mditab1.AdvOfficeTabCount-1 do
  begin
    mditab1.GetChildForm(mditab1.AdvOfficeTabs[i]).Close;
  end;

  mditab1.Visible := False;
end;

procedure Tfrmmain.mditab1TabClose(Sender: TObject; TabIndex: Integer;
  var Allow: Boolean);
begin
  //首页不允许关闭
  if mditab1.GetChildForm(mditab1.AdvOfficeTabs[TabIndex]).Caption = '首页' then
  begin
    Allow := false;
    exit;
  end;
end;

procedure Tfrmmain.addNavBarItems(moksy, zusy: integer;
  pnavbar: PDxNavBar);
var
  navbaritem : TdxNavBarItem;
begin
  //获取当前模块、当前组、当前角色有权限的菜单；
  dmyuang.qry3.OpenSql('select * from sys_caiddy c, sys_jiaosqx q ' +
      ' where c.moksy=' + IntToStr(moksy) +
      '   and c.zusy=' + IntToStr(zusy) +
      '   and c.shifxs=1' +
      '   and q.caidbm=c.bianm' +
      '   and q.jiaosbm=' + IntToStr(gmysystem.loginyuang.jiaosbm) +
      ' order by moksy,zusy,annsy');
            
  //检查当前模块/当前分组是否存在菜单项，不存在则隐藏当前分组；
  if dmyuang.qry3.RecordCount = 0 then
  begin
    pnavbar^.Groups[zusy].Visible := False;
    dmyuang.qry3.Close;

    Exit;
  end;
  
  dmyuang.qry3.First;
  while not dmyuang.qry3.Eof do
  begin
    navbaritem := pnavbar^.Items.Add;
    navbaritem.Caption := dmyuang.qry3.getString('annwb');
    navbaritem.OnClick := OnNavBarItemClick;
    navbaritem.Index := dmyuang.qry3.getInteger('annsy');

    //菜单项图标处理；院长查询全部为报表，其他按照组索引处理
    if(dmyuang.qry3.getInteger('moksy') = 12) then
    begin
      navbaritem.SmallImageIndex := 2;
      navbaritem.LargeImageIndex := 2;
    end
    else
    begin
      navbaritem.SmallImageIndex := dmyuang.qry3.getInteger('zusy');
      navbaritem.LargeImageIndex := dmyuang.qry3.getInteger('zusy');
    end;

    navbaritem.Tag := dmyuang.qry3.getInteger('bianm');
    pnavbar^.Groups[zusy].CreateLink(navbaritem);

    dmyuang.qry3.Next;
  end;
end;

procedure Tfrmmain.initNavBar;
var
  i : integer;
begin

  // mainnavbar
  navbar.OptionsStyle.DefaultStyles.GroupHeader.Font.Name := '宋体';
  navbar.OptionsStyle.DefaultStyles.GroupHeader.Font.Size := StrToIntDef
    (gmysystem.local_sysparams.valueitem('navbar_main_groupheader_fontsize'),
    10);
  navbar.OptionsStyle.DefaultStyles.item.Font.Name := '宋体';
  navbar.OptionsStyle.DefaultStyles.item.Font.Size := StrToIntDef
    (gmysystem.local_sysparams.valueitem('navbar_main_item_fontsize'), 10);

  for i:=0 to self.ComponentCount-1 do
  begin
    if (Components[i] is TDxNavBar) then
    begin
      (Components[i] as TDxNavBar).OptionsStyle.DefaultStyles.GroupHeader.Font.Name := '宋体';
      (Components[i] as TDxNavBar).OptionsStyle.DefaultStyles.GroupHeader.Font.Size := StrToIntDef
        (gmysystem.local_sysparams.valueitem('navbar_main_groupheader_fontsize'), 10);

      (Components[i] as TDxNavBar).OptionsStyle.DefaultStyles.GroupHeaderHotTracked.Font.Name := '宋体';  
      (Components[i] as TDxNavBar).OptionsStyle.DefaultStyles.GroupHeaderHotTracked.Font.Style :=[fsBold];
      (Components[i] as TDxNavBar).OptionsStyle.DefaultStyles.GroupHeaderHotTracked.Font.Size := StrToIntDef
        (gmysystem.local_sysparams.valueitem('navbar_main_groupheader_fontsize'), 10);

      (Components[i] as TDxNavBar).OptionsStyle.DefaultStyles.item.Font.Name := '宋体';
      (Components[i] as TDxNavBar).OptionsStyle.DefaultStyles.item.Font.Size := StrToIntDef
        (gmysystem.local_sysparams.valueitem('navbar_main_item_fontsize'), 10);

      (Components[i] as TDxNavBar).OptionsStyle.DefaultStyles.ItemHotTracked.Font.Name := '宋体';
      (Components[i] as TDxNavBar).OptionsStyle.DefaultStyles.ItemHotTracked.Font.Style := [fsBold];
      (Components[i] as TDxNavBar).OptionsStyle.DefaultStyles.ItemHotTracked.Font.Size := StrToIntDef
        (gmysystem.local_sysparams.valueitem('navbar_main_item_fontsize'), 10);
    end;
  end;

  { 系统管理
  }
  addNavBarItems(0,0,@navxitgl);  
  addNavBarItems(0,1,@navxitgl);
  //模块能否隐藏
  MokNavCanHide(3, @navxitgl);

  { 基础数据
  }
  addNavBarItems(1,0,@navjicsj);
  //模块能否隐藏
  MokNavCanHide(2, @navjicsj);

  { 评审模板制作
  addNavBarItems(2,0,@navshujzd);
  //模块能否隐藏
  MokNavCanHide(2, @navshujzd);
  }

  { 评审管理
  }
  addNavBarItems(3,0,@navyewgl);
  //查询报表
  addNavBarItems(3,1,@navyewgl);
  //模块能否隐藏
  MokNavCanHide(0, @navyewgl);
end;

procedure Tfrmmain.MokNavCanHide(mokIndex: integer; pnavbar: PDxNavBar);
var
  i : Integer;
  bfound : Boolean;
begin
  bfound := false;

  for i:=0 to pnavbar^.Groups.Count-1 do
  begin
    if pnavbar^.Groups[i].LinkCount>0 then
    begin
      bfound := True;
      Break;
    end;
  end;

  if not bfound then
  begin
    navbar.Groups[mokIndex].Visible := False;
  end;
end;

procedure Tfrmmain.OnNavBarItemClick(Sender: TObject);
var
  DllParam : TDllParam;
  p_form : PForm;
begin
  { 调用frmmain窗体菜单项单击事件统一处理dll接口；
    传入菜单id以寻址对应的业务处理函数；
    菜单id在配置sql中已经分配给定；菜单项的显示顺序由listorder字段单独控制；
  }
  MakeDllParams(@dllparam);
  p_form := frmmain_menuitem_click(@dllparam, (sender as TdxNavBarItem).Tag);
  if p_form <> nil then
  begin
    appendChildForm(p_form^);
  end;

  closeNavBar();
end;

procedure Tfrmmain.btn1Click(Sender: TObject);
begin      
  try
    if not Assigned(frmxitgl_xiugkoul) then
      frmxitgl_xiugkoul := Tfrmxitgl_xiugkoul.Create(nil);
    frmxitgl_xiugkoul.Execute_dll_showmodal(@dllparams);
  finally
    frmxitgl_xiugkoul.Free;
    frmxitgl_xiugkoul := nil;
  end;
end;

procedure Tfrmmain.btnquitClick(Sender: TObject);
begin
  close();
end;

procedure Tfrmmain.closeNavBar;
begin
  navbar.Visible := false;
end;

procedure Tfrmmain.toolb1Click(Sender: TObject);
begin
  navbar.Visible := not navbar.Visible;
end;

procedure Tfrmmain.toolbBarButtonExitClick(Sender: TObject);
begin
  close();
end;

procedure Tfrmmain.getSysDatetimeFormat;
var
  buf:pchar;
  i:integer;
begin
  getmem(buf,100);
  i:=100; //i必须在调用前赋值为buf缓冲区的长度。如果设为0或负值，将取不到设置的值
  GetLocaleInfo(LOCALE_USER_DEFAULT,LOCALE_SSHORTDATE,buf,i); //取当前用户设置，短日期格式。
  GPrevShortDate:=string(buf);
  i:=100;
  GetLocaleInfo(LOCALE_USER_DEFAULT,LOCALE_SLONGDATE,buf,i); //取长日期格式
  GPrevLongDate:=string(buf);
  i:=100;
  GetLocaleInfo(LOCALE_USER_DEFAULT,LOCALE_STIMEFORMAT,buf,i); //取时间格式
  GPrevTimeFormat:=string(buf);
  FreeMem(buf);
end;

procedure Tfrmmain.restoreSysDatetimeFormat;
var
  p:DWORD;
begin
  SetLocaleInfo(LOCALE_USER_DEFAULT,LOCALE_SSHORTDATE,pchar(GPrevShortDate)); //短日期
  SetLocaleInfo(LOCALE_USER_DEFAULT,LOCALE_SLONGDATE,pchar(GPrevLongDate));
  SetLocaleInfo(LOCALE_USER_DEFAULT,LOCALE_STIMEFORMAT,pchar(GPrevTimeFormat)); //设置时间

  SendMessageTimeOut(HWND_BROADCAST,WM_SETTINGCHANGE,0,0,SMTO_ABORTIFHUNG,10,p);
  //设置完成后必须调用，通知其他程序格式已经更改，否则即使是程序自身也不能使用新设置的格式
end;

procedure Tfrmmain.setSysDatetimeFormat;
var
  p:DWORD;
begin
  SetLocaleInfo(LOCALE_USER_DEFAULT,LOCALE_SSHORTDATE,pchar('yyyy-MM-dd')); //短日期
  SetLocaleInfo(LOCALE_USER_DEFAULT,LOCALE_SLONGDATE,pchar('yyyy''年''M''月 ''d''日'''));
  SetLocaleInfo(LOCALE_USER_DEFAULT,LOCALE_STIMEFORMAT,pchar('HH:mm:ss')); //设置时间

  SendMessageTimeOut(HWND_BROADCAST,WM_SETTINGCHANGE,0,0,SMTO_ABORTIFHUNG,10,p);
  //设置完成后必须调用，通知其他程序格式已经更改，否则即使是程序自身也不能使用新设置的格式
end;

procedure Tfrmmain.upmessage_leftnavbar_close(var msg: TMSG);
begin
  closeNavBar();
end;

procedure Tfrmmain.upmessage_mdichild_close(var msg: TMSG);
var
  i : Integer;
begin
  //如果打开的mdichild窗体个数=2 then 显示左边导航栏
  case MDIChildCount of
    2:
    begin    
      //打开左边的导航栏
      //不打开导航条了；只有管理员才会使用到导航条这个东西；
      //openNavBar();
    end;
  end;
end;

procedure Tfrmmain.openNavBar;
begin
  navbar.Visible := true;
end;

procedure Tfrmmain.mditab1Change(Sender: TObject);
begin
  if mditab1.GetChildForm(mditab1.AdvOfficeTabs[mditab1.ActiveTabIndex]).Caption = '首页' then
  begin
    mditab1.ButtonSettings.CloseButton := false;

    //不打开导航条了
    //openNavBar;
  end
  else
  begin
    mditab1.ButtonSettings.CloseButton := true;
    closeNavBar;
  end;
end;

procedure Tfrmmain.showSystemMessage(strmessage: string);
begin
  application.ProcessMessages;
  stbmain.Panels[3].Text := strmessage;
  application.ProcessMessages;
end;

procedure Tfrmmain.toolb2Click(Sender: TObject);
var
  appfilename : string;
begin
  { 切换用户
  }
  appfilename := ExtractFilePath(Application.ExeName) + '\\ppdfreview.exe';

  application.Terminate;

  sleep(2000);
  
  ShellExecute(0,'open',PChar(appfilename), '', '', SW_SHOW);
end;

procedure Tfrmmain.toolb3Click(Sender: TObject);
var
  DllParam : TDllParam;
  p_form : PForm;
begin
  { 调用frmmain窗体菜单项单击事件统一处理dll接口；
    传入菜单id以寻址对应的业务处理函数；
    菜单id在配置sql中已经分配给定；菜单项的显示顺序由listorder字段单独控制；
  }
  MakeDllParams(@dllparam);
  p_form := frmmain_menuitem_click(@dllparam, 1501);
  if p_form <> nil then
  begin
    appendChildForm(p_form^);
  end;

  closeNavBar();
end;

end.

