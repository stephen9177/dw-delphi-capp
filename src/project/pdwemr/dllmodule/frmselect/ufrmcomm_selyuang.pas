{ 员工选择窗体
  直接调用，通过选择科室方式过滤员工信息
}
unit ufrmcomm_selyuang;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmdbselectbase, Menus, UpPopupMenu, frxClass, frxDesgn,
  frxDBSet, UpDataSource, DB, ADODB, UpAdoTable, UpAdoQuery, Grids,
  Wwdbigrd, Wwdbgrid, UpWWDbGrid, StdCtrls, UpEdit, UpLabel, Buttons,
  UpSpeedButton, ExtCtrls, UPanel, frxExportXLS, frxExportPDF, ToolPanels,
  UpAdvToolPanel, ComCtrls, UpTreeView, ImgList, udbtree;

type
  Tfrmcomm_selyuang = class(Tfrmdbselectbase)
    p1: TUpAdvToolPanel;
    qryxiangmtree: TUpAdoQuery;
    imglstilimglisttree: TImageList;
    tvxiangm: TUpTreeView;
    procedure edtseljianpKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure tvxiangmChange(Sender: TObject; Node: TTreeNode);
    procedure tvxiangmGetImageIndex(Sender: TObject; Node: TTreeNode);
    procedure btnselectClick(Sender: TObject);
    procedure edtseljianpKeyPress(Sender: TObject; var Key: Char);

  private      
    dbtree : TDBTree;   
    function getnodename_showtreenode() : string;
    //显示目录树获取节点显示名称回调函数
    
    procedure loadyuang(bumbm : integer);
    //加载员工列表
  
  protected                                                    
    function Execute_dll_showmodal_before() : Boolean; override;
    function Execute_dll_showmodal_after() : Boolean; override;
  public
    fret_bianm : string; 
    fret_xingm : string;
  end;

{
var
  frmcomm_selyuang: Tfrmcomm_selyuang;
}

implementation

{$R *.dfm}

{ Tfrmcomm_selyuang }

function Tfrmcomm_selyuang.Execute_dll_showmodal_after: Boolean;
begin
  Result := False;

  if ModalResult = mrCancel then Exit;

  if qry.isEmpty then Exit;

  fret_bianm := qry.fieldbyname('bianm').AsString;
  fret_xingm := qry.fieldbyname('xingm').AsString;
  
  Result := True;
end;

function Tfrmcomm_selyuang.Execute_dll_showmodal_before: Boolean;
begin
  //不采用父类的处理方法
  //inherited;
                 
  //显示项目树
  if not Assigned(dbtree) then
  begin
    //创建树操作对象
    dbtree := TDBTree.Create(qryxiangmtree, 'jc_bum', tvxiangm, dllparams.padoconn);
    dbtree.fgetnodename_showtreenode := getnodename_showtreenode;
  end;
  dbtree.wherestr:= ' where zhuangt=1';
  //显示树
  dbtree.showtree;

  result := true;
end;

procedure Tfrmcomm_selyuang.loadyuang(bumbm: integer);
begin
  if bumbm=0 then
  begin
    qry.OpenSql(format('select * from v_yuang' +
      ' where zhuangt=1', []));
  end
  else
  begin
    qry.OpenSql(format('select * from v_yuang' +
      ' where bumbm=%d' +
      '   and zhuangt=1', [bumbm]));
  end;
end;

procedure Tfrmcomm_selyuang.edtseljianpKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  //inherited;

  if key=13 then
  begin
    if trim(edtseljianp.Text)<>'' then
    begin
      qry.OpenSql(format('select * from v_yuang' +
        ' where denglzh like ''%s%s%s''' +
        '   and zhuangt=1',
        ['%', trim(edtseljianp.Text), '%']));
    end;
  end;
end;

procedure Tfrmcomm_selyuang.tvxiangmChange(Sender: TObject;
  Node: TTreeNode);
begin
  inherited;

  //树当前选中节点改变,通知qry改变当前记录
  dbtree.treechange(node);

  { 过滤项目
  }
  { 只在选中分类下的项目中过滤}
  if tvxiangm.Selected<>nil then
  begin
    qry.OpenSql(format('select * from v_yuang' +
      ' where bumbm=%d' +
      '   and zhuangt=1',
       [qryxiangmtree.getInteger('bianm')]));
  end
  else
  begin
    qry.OpenSql('select * from v_yuang where 1=2');
  end;
end;

function Tfrmcomm_selyuang.getnodename_showtreenode: string;
begin
  Result := qryxiangmtree.fieldbyname('mingc').AsString;
end;

procedure Tfrmcomm_selyuang.tvxiangmGetImageIndex(Sender: TObject;
  Node: TTreeNode);
begin
  inherited;

  { 改变节点图标
  }
  if Node.Selected then
  begin
    Node.ImageIndex := 1;
  end
  else
  begin
    Node.ImageIndex := 2;
  end;
end;

procedure Tfrmcomm_selyuang.btnselectClick(Sender: TObject);
var
  key: Word;
begin
  inherited;

  key:= 13;
  edtseljianpKeyUp(edtseljianp, key, []);
end;

procedure Tfrmcomm_selyuang.edtseljianpKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if key='''' then
  begin
    key:= chr(0);
    exit;
  end;
end;

end.
