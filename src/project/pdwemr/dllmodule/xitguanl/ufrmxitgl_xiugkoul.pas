unit ufrmxitgl_xiugkoul;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmdbdialogbase, Menus, UpPopupMenu, frxClass, frxDesgn,
  frxDBSet, UpDataSource, DB, ADODB, UpAdoTable, UpAdoQuery, Buttons,
  UpSpeedButton, ExtCtrls, UPanel, StdCtrls, UpGroupBox,
  UpLabel, UpEdit, frxExportXLS, frxExportPDF, udmoperator;

type
  Tfrmxitgl_xiugkoul = class(Tfrmdbdialogbase)
    grpxiugkoul: TUpGroupBox;
    lbl1: TUpLabel;
    lbl2: TUpLabel;
    lbl3: TUpLabel;
    edtoldpass: TUpEdit;
    edtnewpass1: TUpEdit;
    edtnewpass2: TUpEdit;
    procedure FormDestroy(Sender: TObject);
    procedure btnyesClick(Sender: TObject);
  private
    dmoperator : Tdmoperator;
  protected
    function Execute_dll_showmodal_before() : Boolean; override;
    function Execute_dll_showmodal_after() : Boolean; override;
  end;

var
  frmxitgl_xiugkoul: Tfrmxitgl_xiugkoul;

implementation

{$R *.dfm}

{ Tfrmdbdialogbase1 }

function Tfrmxitgl_xiugkoul.Execute_dll_showmodal_after: Boolean;
begin
  Result := ModalResult = mrOk;
end;

function Tfrmxitgl_xiugkoul.Execute_dll_showmodal_before: Boolean;
begin
  Result := inherited Execute_dll_showmodal_before;
  
  dmoperator := Tdmoperator.Create(@dllparams);

  Result := True;
end;

procedure Tfrmxitgl_xiugkoul.FormDestroy(Sender: TObject);
begin
  inherited;

  FreeAndNil(dmoperator);
end;

procedure Tfrmxitgl_xiugkoul.btnyesClick(Sender: TObject);
begin
  //inherited;

  //修改口令
  //旧口令是否正确
  if not dmoperator.checkPassword(dllparams.mysystem^.loginyuang.bianm, edtoldpass.Text) then
  begin
    baseobj.showerror('旧口令输入不正确！');
    edtoldpass.SetFocus;
    Exit;
  end;
  //口令不允许为空
  if Trim(edtnewpass1.Text) = '' then
  begin
    baseobj.showerror('口令不允许为空！');
    edtnewpass1.SetFocus;
    Exit;
  end;
  //两次口令是否一致
  if edtnewpass1.Text <> edtnewpass2.Text then
  begin
    baseobj.showerror('两次输入的新口令不一致，请重新输入！');
    edtnewpass1.Text := '';
    edtnewpass2.Text := '';
    edtnewpass1.SetFocus;
    Exit;
  end;

  //修改口令
  if dmoperator.updatePassword(dllparams.mysystem^.loginyuang.bianm, edtnewpass1.Text) then
  begin                
    baseobj.showmessage('口令修改成功！');
    ModalResult := mrOk;  
  end
  else
  begin
    baseobj.showerror('修改密码失败：' + dmoperator.errmsg);
    ModalResult := mrCancel;
  end;
end;

end.
